/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.algormidterm2;

import java.util.Scanner;

/**
 *
 * @author Aritsu
 */
public class Midterm2 {
    
      static void pairSum(int n ,int[] arr , int c){
          // foreach  element in array except last element
          for(int i = 0;i<n-1;i++){
              // loop start from i+1 element untill last element
              for(int j = i+1 ; j < n; j++){
                  
                  if(arr[i] + arr[j] == c ){
                      System.out.println("Pair given sum  "+ c+" is " + "("+ arr[i] + " , "+ arr[j] + ")" );
                  }
              }   
          }
      }
    
    
    
    
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Input length of array : ");
        int n, c;
        n = s.nextInt();
        int[] arr = new int[n];

        System.out.println("Input element in array : ");
        for (int i = 0; i < n; i++) {
            arr[i] = s.nextInt();
        }
        
        System.out.println("Input the target pair sum");
        c = s.nextInt();
        
        pairSum(n, arr, c);
        
    }
}
